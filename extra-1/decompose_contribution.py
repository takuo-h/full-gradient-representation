#! -*- coding:utf-8 -*-

# ------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d/%H-%M-%S.%f')+' '+' '.join(map(str,args)))

# -------------------------------------------------------------------
import torch
def get_contribution(model, x, y):
	assert x.size(0)==1, f'input batch-size must be 1. input-shape:{x.shape}'
	
	model.eval()
	model.zero_grad()

	model_dict = model.state_dict()

	def _get_bias_in_BN(name):
		# batch-normalization : BN = static \ocot dynamic
		#  dynamimc : input -> z = (x-mean)/sqrt(var+epsilon)
		#  static   : z -> output = scale*z  + mean
		#  then, total-bias (i.e., BN(0)) is -scale * mean / sqrt(var+epsilon) + mean
		rm = model_dict[f'{name}.running_mean']
		rv = model_dict[f'{name}.running_var']
		b = model_dict[f'{name}.bias']
		w = model_dict[f'{name}.weight']
		e = 1e-05 # epsilon
		return -w*rm/torch.sqrt(rv+e) + b

	batchnorm_names = set()
	for name in model_dict.keys():
		if name.endswith('.running_mean'):
			batchnorm_names.add(name.rstrip('.running_mean'))

	x = x.requires_grad_()
	p = model(x)
	p = p[0,y]
	p.backward()

	contributions = []

	value = (x*x.grad).sum().item()
	contributions.append(value)
	for name, param in model.named_parameters():
		if name.endswith('.bias'):
			name = name.rstrip('.bias')
			if name in batchnorm_names:
				bias = _get_bias_in_BN(name)
			else:
				bias = param
			value 	= (bias*param.grad).sum().item()
			contributions.append(value)
	return contributions

# -------------------------------------------------------------------

import os
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
def draw(contributions, p, title, file_name):
	report(f'draw:{file_name}')

	fig = plt.figure(figsize=(7,5))
	plt.subplots_adjust(left=0.12, right=0.95, top=0.9, bottom=0.12, wspace=0.2, hspace=0.2)
	ax = plt.subplot(1,1,1)

	accumulations = [sum(contributions[:i+1]) for i in range(len(contributions))]

	L		= len(contributions)
	layers	= range(L)

	ax.plot(layers, contributions,	color='blue',  lw=0.5, zorder=10, linestyle='--',	label='contribution')
	ax.plot(layers, accumulations,	color='black', lw=1.5, zorder=10, linestyle='-', 	label='sum-of-contribution')
	ax.scatter([L-1],[p],	color='red', s=30,zorder=9, label='true-output')

	ax.legend(fontsize=15, facecolor='white',framealpha=1.0)

	# x-ticks
	L = len(layers)
	xticks = range(0,L+1,L//10)
	for b in xticks:
		ax.axvline(x=b, color='black',lw=0.5,zorder=-3, linestyle='-')
	ax.set_xticks(xticks)
	ax.set_xticklabels(map(str,xticks), fontsize=10)
	ax.get_xaxis().set_major_formatter(ticker.ScalarFormatter())

	# y-ticks
	ax.axhline(y=0, color='black',lw=0.5,zorder=-3, linestyle='-')

	ax.minorticks_off()

	# lim, label and title
	ax.set_xlim([-1,L])
	ax.set_xlabel('layer',fontsize=15)
	ax.set_ylabel('contributions',fontsize=15)
	ax.set_title(title,fontsize=20)

	os.makedirs(os.path.dirname(file_name),exist_ok=True)
	plt.savefig(file_name,dpi=120)
	plt.clf();plt.cla();plt.close()



# -------------------------------------------------------------------
from PIL import Image
from torchvision import transforms
def gen_dataset(config):
	transform = []
	transform.append(transforms.Resize((224,224)))
	transform.append(transforms.ToTensor())
	transform.append(transforms.Normalize(mean = [0.485, 0.456, 0.406],std = [0.229, 0.224, 0.225]))
	transform = transforms.Compose(transform)

	files	= ['apple.jpg','banana.jpg','orange.jpg']
	labels	= [948, 954, 950]
	for f,y in zip(files, labels):
		x = Image.open(f'{config.data_dir}/{f}')	# load image
		x = transform(x)							# pre-process
		x.unsqueeze_(0)								# convert into one-batch
		f = f.replace('.jpg','')
		yield f,x,y

# -------------------------------------------------------------------
from argparse import ArgumentParser
def get_config():
	config = ArgumentParser()
	config.add_argument('--data_dir',					default='DATAs',	type=str)
	config.add_argument('--output_dir',					default='OUTPUTs',	type=str)
	config.add_argument('--target_model',	'-model',	default='resnet18',	type=str, choices=[f'resnet{L}' for L in [18,34,50,101,152]])
	return config.parse_args()

# -------------------------------------------------------------------
def main():
	config	= get_config()
	model 	= torch.hub.load('pytorch/vision:v0.5.0', config.target_model, pretrained=True)

	for fruit,x,y in gen_dataset(config):
		contributions	= get_contribution(model, x, y)
		output 			= model(x)[0,y].item()
		title		=  f'Model:{config.target_model}  File:{fruit}'
		file_name	= f'{config.output_dir}/{config.target_model}/{fruit}.png'
		draw(contributions, output, title, file_name)

# -------------------------------------
if __name__ == '__main__':
	main()
















# -------------------------------------