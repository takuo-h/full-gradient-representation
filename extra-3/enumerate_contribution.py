#! -*- coding:utf-8 -*-

# ------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d/%H-%M-%S.%f')+' '+' '.join(map(str,args)))

# -------------------------------------------------------------------
import copy
import torch
def get_contribution_y(model, x, y):
	assert x.size(0)==1, f'input batch-size must be 1. input-shape:{x.shape}'
	
	model.eval()
	model.zero_grad()

	model_dict = model.state_dict()

	def _get_bias_in_BN(name):
		# batch-normalization : BN = static \ocot dynamic
		#  dynamimc : input -> z = (x-mean)/sqrt(var+epsilon)
		#  static   : z -> output = scale*z  + mean
		#  then, total-bias (i.e., BN(0)) is -scale * mean / sqrt(var+epsilon) + mean
		rm = model_dict[f'{name}.running_mean']
		rv = model_dict[f'{name}.running_var']
		b = model_dict[f'{name}.bias']
		w = model_dict[f'{name}.weight']
		e = 1e-05 # epsilon
		return -w*rm/torch.sqrt(rv+e) + b

	batchnorm_names = set()
	for name in model_dict.keys():
		if name.endswith('.running_mean'):
			batchnorm_names.add(name.rstrip('.running_mean'))

	x = x.requires_grad_()
	p = model(x)
	p = p[0,y]
	p.backward()

	contributions = []

	value = (x*x.grad).sum().item()
	contributions.append(value)
	for name, param in model.named_parameters():
		if name.endswith('.bias'):
			name = name.rstrip('.bias')
			if name in batchnorm_names:
				bias = _get_bias_in_BN(name)
			else:
				bias = param
			value 	= (bias*param.grad).sum().item()
			contributions.append(value)
	return contributions



def get_all_contribution(model,x,label_size=1000):
	allcontributions = {}
	for i in range(label_size):
		if i%100==0: report(f'\tworking label:{i}/{label_size}')
		allcontributions[i] = get_contribution_y(model,x,i)
	return allcontributions

# -------------------------------------------------------------------

import matplotlib.cm as cm
from collections import defaultdict
def get_trends(scores):
	trends = defaultdict(list)
	for label in scores:
		for layer,value in enumerate(scores[label]):
			trends[layer].append(value)
	bundle = []
	for layer in trends:
		y = trends[layer]
		y = sorted(y)
		L = len(y)
		x = []
		for i in range(L):
			x.append(layer+(i-L/2)/(5*L))
		c = [cm.seismic(float(i)/L) for i in range(L)]
		bundle.append([x,y,c])
	return bundle

import os
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.colors as colors
import matplotlib.colorbar as colorbar
import matplotlib.axes_grid1 as axes_grid1
def draw(allcontributions, y, title, file_name):
	report(f'draw:{file_name}')

	fig = plt.figure(figsize=(8,3))
	plt.subplots_adjust(left=0.10, right=0.94, top=0.87, bottom=0.19, wspace=0.2, hspace=0.4)

	ax = plt.subplot(1,1,1)

	for dx,dy,dc in get_trends(allcontributions):
		ax.scatter(dx,dy,c=dc,marker='.',s=4.0, edgecolor='none')
		ax.plot(dx, dy, lw=0.1, zorder=-6, color='black')

	layers = range(len(allcontributions[y]))
	ax.plot(layers, allcontributions[y], lw=1.0, linestyle='-', zorder=10, alpha=1.0, color='green', label='true')

	# set grid manually
	L = len(layers)
	xticks = range(0,L+1,L//10)
	for b in xticks:
		ax.axvline(x=b, color='black',lw=0.5,zorder=-3, linestyle='-')
	ax.set_xticks(xticks)
	ax.set_xticklabels(map(str,xticks), fontsize=10)
	ax.get_xaxis().set_major_formatter(ticker.ScalarFormatter())

	ax.axhline(y=0, color='black',lw=0.5,zorder=-3, linestyle='-')

	# set other settings
	ax.set_xlim([-1,len(layers)])
	ax.set_xlabel('layer',fontsize=13)
	ax.set_ylabel('contributions',fontsize=13)
	ax.set_title(title,fontsize=13)
	ax.legend(fontsize=13,framealpha=1.0)

	# set colorbar
	divider = axes_grid1.make_axes_locatable(plt.gca())
	ax_cb = divider.new_horizontal(size="3%", pad=0.20)

	N = 100
	intervals		= [float(i)/N for i in range(1,N)]
	intervalcolor	= colors.ListedColormap([cm.seismic(v) for v in intervals])
	
	ticks = [i for i in range(1,N)]
	boundaries = [0] + ticks
	boundaries = [0.5+t for t in boundaries]
	cb2 = colorbar.ColorbarBase(ax_cb, cmap=intervalcolor, boundaries=boundaries, ticks=ticks, orientation='vertical')

	selected_ticks	= list(range(10,N,10))
	selected_labels	= [f'{i:02d}%' for i in selected_ticks]
	cb2.set_ticks(selected_ticks)
	cb2.set_ticklabels(selected_labels)

	cb2.minorticks_off()
	plt.gcf().add_axes(ax_cb)

	os.makedirs(os.path.dirname(file_name),exist_ok=True)
	plt.savefig(file_name,dpi=120)
	plt.clf();plt.cla();plt.close()



# -------------------------------------------------------------------
from PIL import Image
from torchvision import transforms
def gen_dataset(config):
	transform = []
	transform.append(transforms.Resize((224,224)))
	transform.append(transforms.ToTensor())
	transform.append(transforms.Normalize(mean = [0.485, 0.456, 0.406],std = [0.229, 0.224, 0.225]))
	transform = transforms.Compose(transform)

	files	= ['apple.jpg','banana.jpg','orange.jpg']
	labels	= [948, 954, 950]
	for f,y in zip(files, labels):
		x = Image.open(f'{config.data_dir}/{f}')	# load image
		x = transform(x)							# pre-process
		x.unsqueeze_(0)								# convert into one-batch
		f = f.replace('.jpg','')
		yield f,x,y

# -------------------------------------------------------------------
from argparse import ArgumentParser
def get_config():
	config = ArgumentParser()
	config.add_argument('--data_dir',					default='DATAs',	type=str)
	config.add_argument('--output_dir',					default='OUTPUTs',	type=str)
	config.add_argument('--target_model',	'-model',	default='resnet18',	type=str, choices=[f'resnet{L}' for L in [18,34,50,101,152]])
	return config.parse_args()

# -------------------------------------------------------------------
import os
import pickle
def main():
	report('start')
	config	= get_config()
	model 	= torch.hub.load('pytorch/vision:v0.5.0', config.target_model, pretrained=True)

	for fruit,x,y in gen_dataset(config):
		report(f'\tstart:{fruit}')

		# calculate all labels' contributions 
		contributions	= get_all_contribution(model, x)

		title		=  f'Contributions  Model:{config.target_model}  File:{fruit}'
		file_name	= f'{config.output_dir}/{config.target_model}/{fruit}-contributions.png'
		draw(contributions, y, title, file_name)
		
		# accumlate contributions
		accumulations = {}
		for k,v in contributions.items():
			accumulations[k] = [sum(v[:i+1]) for i in range(len(v))]

		title		=  f'Accumulations  Model:{config.target_model}  File:{fruit}'
		file_name	= f'{config.output_dir}/{config.target_model}/{fruit}-accumulations.png'
		draw(accumulations, y, title, file_name)
		
	report('finish')


# -------------------------------------
if __name__ == '__main__':
	main()
















# -------------------------------------