#! -*- coding:utf-8 -*-

# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d/%H:%M:%S.%f')+' '+' '.join(map(str,args)).replace('\n',''))

# -------------------------------------

import torch
from torchvision import transforms
class NormalizeInverse(transforms.Normalize):
	def __init__(self, mean, std):
		mean = torch.as_tensor(mean)
		std = torch.as_tensor(std)
		std_inv = 1 / (std + 1e-7)
		mean_inv = -mean * std_inv
		super(NormalizeInverse, self).__init__(mean=mean_inv, std=std_inv)

	def __call__(self, tensor):
		return super(NormalizeInverse, self).__call__(tensor.clone())
denormalize = NormalizeInverse(mean = [0.485, 0.456, 0.406], std = [0.229, 0.224, 0.225])

# -------------------------------------------------------------------
import os
import cv2
import numpy as np
import matplotlib.pyplot as plt
def draw(x, file_name):
	if len(x.shape)==4: x = x[0]
	x = denormalize(x)
	x = x.squeeze(0).numpy()
	x = np.uint8(x * 255).transpose(1,2,0)
	x = cv2.resize(x, (224, 224))
	os.makedirs(os.path.dirname(file_name),exist_ok=True)	
	cv2.imwrite(file_name, x[:,:,[2, 1, 0]])

# -------------------------------------------------------------------
from torch.autograd import Variable
import torch.nn.functional as F
def get_direction(model, x,y, config):
	if y is None:
		p = model(x)
		t = p[0].data.max(0)[1].item()
		t = torch.tensor([t],dtype=torch.long)
	else:
		y = torch.tensor([y],dtype=torch.long)

	get_loss = torch.nn.CrossEntropyLoss()
	d = torch.randn(x.shape)
	if 0<=config.core: d = d.cuda()
	for _ in range(config.iteration):
		d = F.normalize(d, eps=config.normalize_eps)
		d = Variable(d, requires_grad=True)
		p = model(x + config.xi*d)
		if y is None:	loss = get_loss(p,t)
		else:			loss = get_loss(p,y)
		model.zero_grad()
		loss.backward()
		d = d.grad.data.clone()
	d = F.normalize(d, eps=config.normalize_eps)
	d = Variable(d, requires_grad=False)
	return d

# -------------------------------------------------------------------
import torch
from torchvision import datasets
from torchvision import transforms
def load_dataset(image_dir='DATAs'):
	transform = []
	transform.append(transforms.Resize((224,224)))
	transform.append(transforms.ToTensor())
	transform.append(transforms.Normalize(mean = [0.485, 0.456, 0.406],std = [0.229, 0.224, 0.225]))
	transform = transforms.Compose(transform)
	data = datasets.ImageFolder(image_dir, transform=transform)
	data = torch.utils.data.DataLoader(data, batch_size=1, shuffle=False)
	return data

import torch
def set_device(config):
	if 0<=config.core<torch.cuda.device_count() and torch.cuda.is_available():
		report(f'use GPU; core:{config.core}')
		torch.cuda.set_device(config.core)
	else:
		report('use CPU in this trial')
		config.core = -1

# -------------------------------------------------------------------
from argparse import ArgumentParser
def get_config():
	config = ArgumentParser()
	# dirs
	config.add_argument('--output_dir',					default='OUTPUTs',	type=str)
	# target model
	config.add_argument('--target_model',	'-model',	default='resnet18',	type=str, choices=[f'resnet{L}' for L in [18,34,50,101,152]])
	config.add_argument('--core',			'-core',	default=-1,			type=int)
	# options for virtual-adversarial-training
	config.add_argument('--iteration',		'-itr',		default=10,			type=int)
	config.add_argument('--normalize_eps',				default=1e-10,		type=float)
	config.add_argument('--xi',				'-xi',		default=1e-1,		type=float)
	config.add_argument('--max_epsilon',	'-maxeps',	default=1e-1,		type=float)
	config.add_argument('--num_epsilon',	'-numeps',	default=11,			type=int)
	return config.parse_args()

# -------------------------------------------------------------------
"""
hessianをpower-methodで解いてadversarial-exampleを作るだけ
	contributionに依存せず
"""
import torch
import numpy as np
def main():
	report('start gen-adversarial')
	config	= get_config()
	model 	= torch.hub.load('pytorch/vision:v0.5.0', config.target_model, pretrained=True)
	model.eval()
	config.output_dir = f'{config.output_dir}/{config.target_model}/'
		
	set_device(config)
	if 0<=config.core: model = model.cuda()

	data = load_dataset()
	for i, (x, y) in enumerate(data):
		report(data.dataset.classes[y],i)
		name = data.dataset.classes[y]
		if name=='apple':	y = 948
		if name=='banana':	y = 954
		if name=='orange':	y = 950
		report(f'\tworking:{name}-{i}')

		d = get_direction(model, x,y, config)

		result_dir = f'{config.output_dir}/{name}-{i:02d}'

		draw(x, f'{result_dir}/original.png')
		draw(d, f'{result_dir}/direction.png')

		epsilons = np.linspace(0, config.max_epsilon, config.num_epsilon)
		for eps in epsilons:
			noisy		= x+eps*d
			file_name	= f'{result_dir}/attacked-images/epsilon={eps:04.3f}.png'
			draw(noisy, file_name)
	report('finish')
	print('-'*50)

# -------------------------------------

if __name__ == '__main__':
	main()
















# -------------------------------------