#! -*- coding:utf-8 -*-

# ------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d/%H-%M-%S.%f')+' '+' '.join(map(str,args)))

# -------------------------------------------------------------------
import torch
def get_contribution(model, x, y):
	assert x.size(0)==1, f'input batch-size must be 1. input-shape:{x.shape}'
	
	model.eval()
	model.zero_grad()

	model_dict = model.state_dict()

	def _get_bias_in_BN(name):
		# batch-normalization : BN = static \ocot dynamic
		#  dynamimc : input -> z = (x-mean)/sqrt(var+epsilon)
		#  static   : z -> output = scale*z  + mean
		#  then, total-bias (i.e., BN(0)) is -scale * mean / sqrt(var+epsilon) + mean
		rm = model_dict[f'{name}.running_mean']
		rv = model_dict[f'{name}.running_var']
		b = model_dict[f'{name}.bias']
		w = model_dict[f'{name}.weight']
		e = 1e-05 # epsilon
		return -w*rm/torch.sqrt(rv+e) + b

	batchnorm_names = set()
	for name in model_dict.keys():
		if name.endswith('.running_mean'):
			batchnorm_names.add(name.rstrip('.running_mean'))

	x = x.requires_grad_()
	p = model(x)
	p = p[0,y]
	p.backward()

	contributions = []

	value = (x*x.grad).sum().item()
	contributions.append(value)
	for name, param in model.named_parameters():
		if name.endswith('.bias'):
			name = name.rstrip('.bias')
			if name in batchnorm_names:
				bias = _get_bias_in_BN(name)
			else:
				bias = param
			value 	= (bias*param.grad).sum().item()
			contributions.append(value)
	return contributions


# -------------------------------------------------------------------
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.cm as cm
def draw(contributions, title, file_name):
	report(f'draw:{file_name}')

	epsilons = contributions.keys()
	epsilons = list(sorted(list(epsilons)))


	fig = plt.figure(figsize=(7,4))
	plt.subplots_adjust(left=0.1, right=0.90, top=0.9, bottom=0.15, wspace=0.2, hspace=0.2)
	ax = plt.subplot(1,1,1)

	for i,epsilon in enumerate(epsilons):
		p, scores = contributions[epsilon]
		accumulates = [sum(scores[:i+1]) for i in range(len(scores))]

		ticks = range(len(scores))
		ax.plot(ticks, accumulates,	color=cm.viridis(float(i)/len(epsilons)), lw=0.5)
		ax.scatter([len(ticks)-1],[p], color=cm.viridis(float(i)/len(epsilons)), s=30)

	ax.grid()
	ax.set_xlim([-1,len(ticks)+1])
	ax.set_xlabel('layer', fontsize=15)
	ax.set_ylabel('sum-of-contribution', fontsize=15)
	ax.set_title(title, fontsize=15)


	# set colorbar
	divider = make_axes_locatable(plt.gca())
	ax_cb = divider.new_horizontal(size="3%", pad=0.15)    

	num_epsilon = len(epsilons)+1
	data = [float(i)/num_epsilon for i in range(1,num_epsilon)][::-1]
	cmap = mpl.colors.ListedColormap([cm.viridis(v) for v in data])

	ticks = [i for i in range(1,num_epsilon)]
	boundaries = [0] + ticks
	boundaries = [0.5+t for t in boundaries]
	cb2 = mpl.colorbar.ColorbarBase(ax_cb, cmap=cmap, boundaries=boundaries, ticks=ticks, orientation='vertical')
	labels = [f'{e:04.3f}' for e in epsilons][::-1]
	cb2.set_ticklabels(labels)
	plt.gcf().add_axes(ax_cb)

	ax_cb.text(2.5,1.02,f'epsilon', fontsize=9, horizontalalignment='center',transform=ax_cb.transAxes, zorder=0)

		
	os.makedirs(os.path.dirname(file_name),exist_ok=True)
	plt.savefig(file_name,dpi=120)
	plt.clf();plt.cla();plt.close()










# -------------------------------------------------------------------
import torch
from PIL import Image
from torchvision import transforms
import torchvision.transforms.functional as TF

def gen_dataset(image_dir):
	transform = []
	transform.append(transforms.Resize((224,224)))
	transform.append(transforms.ToTensor())
	transform.append(transforms.Normalize(mean = [0.485, 0.456, 0.406],std = [0.229, 0.224, 0.225]))
	transform = transforms.Compose(transform)
	for epsilon in sorted(os.listdir(image_dir)):
		if '.DS' in epsilon: continue
		x = Image.open(f'{image_dir}/{epsilon}')
		x = transform(x)
		x.unsqueeze_(0)
		epsilon = epsilon.replace('.png','')
		epsilon = epsilon.replace('epsilon=','')
		epsilon = float(epsilon)
		yield epsilon,x


# -------------------------------------------------------------------
from argparse import ArgumentParser
def get_config():
	config = ArgumentParser()
	config.add_argument('--output_dir',					default='OUTPUTs',	type=str)
	config.add_argument('--target_model',	'-model',	default='resnet18',	type=str, choices=[f'resnet{L}' for L in [18,34,50,101,152]])
	return config.parse_args()


# -------------------------------------------------------------------
import os
import numpy as np
def main():
	report('start get-contribution')
	config	= get_config()
	model	= torch.hub.load('pytorch/vision:v0.5.0', config.target_model, pretrained=True)
	model.eval()
	config.output_dir = f'{config.output_dir}/{config.target_model}/'
	
	for d in sorted(os.listdir(config.output_dir)):
		if '.DS' in d: continue

		report(f'\tworking:{d}')
		contributions = {}

		if 'apple' in d:	y = 948
		if 'banana' in d:	y = 954
		if 'orange' in d:	y = 950

		target_dir = f'{config.output_dir}/{d}/attacked-images/'
		for epsilon, x in gen_dataset(target_dir):
			p = model(x)[0,y].item()
			trib = get_contribution(model, x, y)
			contributions[epsilon] = [p,trib]

		file_name	= f'{config.output_dir}/{d}/contributions.png'
		title		=  f'Model:{config.target_model}  File:{d}'
		draw(contributions, title, file_name)
	report('finish')
	print('-'*50)

# -------------------------------------
if __name__ == '__main__':
	main()
















# -------------------------------------