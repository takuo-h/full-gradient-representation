#!/bin/bash

<< COMMENTOUT
1. gen_adversarial.py \
	[virtual-adversarial-training](https://arxiv.org/abs/1507.00677)で強さepsilonのノイズを生成．他の図のepsilonもこれ

2. get_diff.py \
	ノイズを加えた画像と真の値の差を描写．これを独立させてるのは，一度出力した画像での差分を見たいため

3. get_analysis.py \
	ノイズを加えたデータが、どう予測を間違えるかを可視化．違うepsilonごとにこれを計算		

4. get_contribution.py \
	Full-Gradientを用いると出力は各レイヤの和に分解できる．この時の各レイヤの貢献を可視化する．特に違うepsilonごとにこれを計算する．
COMMENTOUT

model='resnet18' # other choices are 18, 34, 50, 101 and 152
python -u gen_adversarial.py -model $model
python -u get_diff.py -model $model
python -u get_analysis.py -model $model
python -u get_contribution.py -model $model

model='resnet152' # other choices are 18, 34, 50, 101 and 152
python -u gen_adversarial.py -model $model
python -u get_diff.py -model $model
python -u get_analysis.py -model $model
python -u get_contribution.py -model $model
