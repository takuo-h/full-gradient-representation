#! -*- coding:utf-8 -*-

# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d/%H-%M-%S.%f')+' '+' '.join(map(str,args)))

# -------------------------------------------------------------------
import numpy as np
import matplotlib.pyplot as plt
import cv2
def draw_image(x, file_name):
	if len(x.shape)==4: x = x[0]
	x = x.squeeze(0).numpy()
	x = np.uint8(x * 255).transpose(1,2,0)
	x = cv2.resize(x, (224, 224))
	x = x[:,:,[2, 1, 0]]
	os.makedirs(os.path.dirname(file_name),exist_ok=True)	
	cv2.imwrite(file_name, x)


# -------------------------------------------------------------------


import torch
from PIL import Image
from torchvision import transforms
import torchvision.transforms.functional as TF
def get_dataset(image_dir):
	transform = []
	transform.append(transforms.Resize((224,224)))
	transform.append(transforms.ToTensor())
	transform = transforms.Compose(transform)
	data = {}
	for epsilon in sorted(os.listdir(image_dir)):
		if '.DS' in epsilon: continue
		x = Image.open(f'{image_dir}/{epsilon}')
		x = transform(x)
		x.unsqueeze_(0)
		epsilon = epsilon.replace('.png','')
		epsilon = epsilon.replace('epsilon=','')
		epsilon = float(epsilon)		
		data[epsilon] = x
	return data

# -------------------------------------------------------------------
from argparse import ArgumentParser
def get_config():
	config = ArgumentParser()
	config.add_argument('--output_dir',					default='OUTPUTs',	type=str)
	config.add_argument('--target_model',	'-model',	default='resnet18',	type=str, choices=[f'resnet{L}' for L in [18,34,50,101,152]])
	return config.parse_args()

# -------------------------------------------------------------------
"""
trueとattacked-imageの差を記述する
	contributionには依存せず
"""
import os
import numpy as np
def main():
	report('start get-diff')
	config	= get_config()
	config.output_dir = f'{config.output_dir}/{config.target_model}/'
	
	for d in sorted(os.listdir(config.output_dir)):
		if '.DS' in d: continue
		report(f'\tworking:{d}')

		target_dir = f'{config.output_dir}/{d}/attacked-images/'
		data = get_dataset(target_dir)

		for eps in data:
			if eps==0: continue
			x = (data[eps]-data[0]).abs()

			file_name = f'{config.output_dir}/{d}/diff/{eps:04.3f}.png'
			draw_image(x, file_name)
	report('finish')
	print('-'*50)

# -------------------------------------
if __name__ == '__main__':
	main()
















# -------------------------------------