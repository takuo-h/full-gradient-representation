# 1-概要

- 次の論文の再実装等をした \
  [Full-Gradient Representation for Neural Network Visualization](https://papers.nips.cc/paper/8666-full-gradient-representation-for-neural-network-visualization) \
  全実験はrun.py or run.shを実行することで再現可能

- この論文は何？ \
	区間線形な活性化関数(例えばReLU)を持つNNの出力が、
$`
f(\mathbf{x}) = 
	\sum_{l}^L
		\langle 
			\mathbf{b}_l
			,
			\nabla_{\mathbf{b}_l} f(\mathbf{x})
		\rangle 
	+
	\langle 
		\mathbf{x}
		,
		\nabla_{\mathbf{x}} f(\mathbf{x})
	\rangle 
`$と分解できることを利用して、より望ましいsaliency mapの構成方法を提案した

- 本pageでは$`\langle \mathbf{b}_l,\nabla_{\mathbf{b}_l} f(\mathbf{x})\rangle `$や
$`\langle \mathbf{x},\nabla_{\mathbf{x}} f(\mathbf{x})
\rangle `$の値を貢献(contribution)と呼ぶ．


# 2-実験

#### 実験:Replciation
- 提案手法のsaliency-mapを出力する

元画像 | overlay | salienty-map |
-------|---------------|---------------|
<img src="replication/DATAs/apple/imagenet/1200081464_30fae875f0.jpg"  width="120" height="120"> | <img src="replication/SALIENCY-MAPs/apple/000-overlay.png"  width="120" height="120"> | <img src="replication/SALIENCY-MAPs/apple/000-saliency.png"  width="120" height="120"> |

-----------------

#### 実験:Extra-1
- NNの出力$`f(\mathbf{x})`$が$`\langle \mathbf{b}_l,\nabla_{\mathbf{b}_l} f(\mathbf{x}) \rangle`$の和で書ける場合、それぞれのレイヤがどのように貢献するかを可視化する．

元画像 | 分解した貢献 |
-------|---------------|
<img src="extra-1/DATAs/banana.jpg"  width="120" height="120"> | <img src="extra-1/OUTPUTs/resnet18/banana.png"  width="240" height="120"> |

-----------------


#### 実験:Extra-2
- モデルはResNet18とResNet152の2つが対象
- ノイズに対し、貢献がどう影響を受けるかを見る \
  具体的には、次を確認する
  - (1) どうnoisyが与えられるか
  - (2) どうラベルを間違えるか
  - (3) 貢献がどう変化するか
- ノイズの作成方法
  - virtual-adversarial-trainingを用いて作成
  - 特にノイズの強さepsilonを変えて影響を見る

<!--
元画像 |
-------|
<img src="extra-2/OUTPUTs/resnet18/orange-02/original.png"  width="120" height="120"> |
-->

モデル | ノイズ有(1) | ノイズが予測に与える影響(2) | 出力の分解(3) |
:-------:|:---------------:|:---------------:|:-------:|
ResNet18 | <img src="extra-2/OUTPUTs/resnet18/orange-02/attacked-images/epsilon=0.100.png"  width="120" height="120"> | <img src="extra-2/OUTPUTs/resnet18/orange-02/scores.png"  width="240" height="120"> | <img src="extra-2/OUTPUTs/resnet18/orange-02/contributions.png"  width="240" height="120"> |
ResNet152 | <img src="extra-2/OUTPUTs/resnet152/orange-02/attacked-images/epsilon=0.100.png"  width="120" height="120"> | <img src="extra-2/OUTPUTs/resnet152/orange-02/scores.png"  width="240" height="120"> | <img src="extra-2/OUTPUTs/resnet152/orange-02/contributions.png"  width="240" height="120"> |


-----------------

#### 実験:Extra-3
- 本手法は、予測するラベルが違えばその貢献は違ってくる
  - 正しいラベルとそうでないラベルで貢献に違いはあるのか？
  - Extra-1の実験を全てのラベルについて行うでこれを可視化する
- 図の説明
  - 正解ラベルの値は緑 (下図はリンゴ)
  - 赤色か青色の点は正解でないラベルの値
  - 全体として累積密度分布を表示する

contributionの分布と正解ラベルの推移 | 累積contributionの分布と正解ラベルの推移 |
-------|-------|
<img src="extra-3/OUTPUTs/resnet18/apple-contributions.png"  width="240" height="120"> | <img src="extra-3/OUTPUTs/resnet18/apple-accumulations.png"  width="240" height="120"> | 

-----------------
-----------------
-----------------

# 3-出力の分解
NN$`f`$の活性化関数が全て区間線形の場合、出力$`f(\mathbf{x})`$は次のように分割することができる．ただし$`\mathbf{b}_l`$は各レイヤのbias項である．


```math
f(\mathbf{x}) = 
	\sum_{l}^L
		\langle 
			\mathbf{b}_l
			,
			\nabla_{\mathbf{b}_l} f(\mathbf{x})
		\rangle 
	+
	\langle 
		\mathbf{x}
		,
		\nabla_{\mathbf{x}} f(\mathbf{x})
	\rangle 
```

-----------------

### イメージを掴む 
- 全結合の場合を具体的に書き下してみて、イメージを掴む

####  全体の流れ 

- 次の性質を利用する．ただし$`\odot`$は要素ごとの積．\
  [A] $`\langle \mathbf{x} \odot \mathbf{y}, \mathbf{z}\rangle = \langle \mathbf{x}, \mathbf{y} \odot \mathbf{z}\rangle`$ \
  [B] $`\langle \mathbf{x}, \bm{A} \mathbf{y} \rangle = \langle \bm{A}^T \mathbf{x}, \mathbf{y} \rangle`$ \
  [C] $`ReLU(\mathbf{x}) = \mathbf{x} \odot \frac{\partial ReLU(\mathbf{x})}{\partial \mathbf{x}}`$


- $`f(\mathbf{x})=\langle \mathbf{w}, \mathbf{x}_L\rangle+c`$とする．ただし$`\mathbf{x}_l = ReLU(\mathbf{z}_l)`$, $`\mathbf{z}_l = \bm{A}_l \mathbf{x}_{l-1} + \mathbf{b}_l`$とする．

  まず$`\nabla_{c} f(\mathbf{x})=1`$なので$`\langle c, \nabla_{c} f(\mathbf{x}) \rangle`$が成り立つ．\
  $`\langle \mathbf{w}, \mathbf{x}_L\rangle`$は次のように書き換えることができる．

```math
\begin{aligned}
\langle \mathbf{w}, \mathbf{x}_L \rangle 
	&= \langle \mathbf{w}, \frac{\partial \mathbf{x}_L}{\partial \mathbf{z}_L} \odot \mathbf{z}_L \rangle \because [C] \\
	&= \langle \mathbf{w} \odot \frac{\partial \mathbf{x}_L}{\partial \mathbf{z}_L}, \mathbf{z}_L \rangle \because [A] \\
	&= \langle \mathbf{w} \odot \frac{\partial \mathbf{x}_L}{\partial \mathbf{z}_L}, \bm{A}_L \mathbf{x}_{L-1} + \mathbf{b}_L \rangle \\
	&= 
		\langle \mathbf{w} \odot \frac{\partial \mathbf{x}_L}{\partial \mathbf{z}_L}, \bm{A}_L \mathbf{x}_{L-1} \rangle 
		+
		\langle \mathbf{w} \odot \frac{\partial \mathbf{x}_L}{\partial \mathbf{z}_L}, \mathbf{b}_L \rangle \\
	&= 
		\langle \bm{A}_L^T (\mathbf{w} \odot \frac{\partial \mathbf{x}_L}{\partial \mathbf{z}_L}),  \mathbf{x}_{L-1} \rangle 
		+
		\langle \mathbf{w} \odot \frac{\partial \mathbf{x}_L}{\partial \mathbf{z}_L}, \mathbf{b}_L  \rangle \because [B]
\end{aligned}
```

これを繰り返すことにより、Back-propagationと同じ値が作られ、その過程で$`\mathbf{b}_l`$に関する値が吐き出される．
特に$`\frac{\partial \mathbf{z}_l}{\partial \mathbf{b}_l}=\mathbb{I}`$なので、例えば上記の場合だと
$`\langle \mathbf{w} \odot \frac{\partial \mathbf{x}_L}{\partial \mathbf{z}_L}, \mathbf{b}_L  \rangle=\langle \mathbf{w} \odot \frac{\partial \mathbf{x}_L}{\partial \mathbf{z}_L} \frac{\partial \mathbf{z}_L}{\partial \mathbf{b}_L}, \mathbf{b}_L  \rangle=\langle \nabla_{\mathbf{b}_L}f(\mathbf{x}),\mathbf{b}_L \rangle`$となる．

結局、[C]でReLUがその微分との要素積で表現できることが重要で、基本的にあとは線形代数の枠組みで分解が成立する．
また[A]は、隠れ層に対するmaskは出力の重みへのmaskに変えても成立することにつながっている．

### 一般の場合

- Biasの無い場合 \
	これはReLUが
		$`\forall \alpha >0, ReLU(\alpha x) = \alpha ReLU(x)`$
	である性質を活用し、
		$`f((1+\epsilon)\mathbf{x}) = f(\mathbf{x}) + \epsilon f(\mathbf{x})`$
	が成立すること、
	テイラー展開
		$`f((1+\epsilon)\mathbf{x}) = f(\mathbf{x}) + \epsilon \langle \mathbf{x}, \nabla_{\mathbf{x}}f(\mathbf{x}) \rangle `$
	を利用すれば確認できるる．

- Biasのある場合 \
	まだ

-----------------
