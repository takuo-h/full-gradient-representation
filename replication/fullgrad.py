import torch
import torch.nn as nn
import torch.nn.functional as F
from math import isclose


class FullGrad():
	def __init__(self, model, im_size = (3,224,224)):
		self.model = model
		self.im_size = (1,) + im_size
		self.model.eval()
		self.blockwise_biases = self.model.getBiases()

	def fullGradientDecompose(self, image, target_class=None):
		image = image.requires_grad_()
		out, features = self.model.getFeatures(image)

		if target_class is None:
			target_class = out.data.max(1, keepdim=True)[1]

		agg = out[0,target_class[0]]

		self.model.zero_grad()
		gradients = torch.autograd.grad(outputs = agg, inputs = features, only_inputs=True)

		input_gradient = gradients[0]

		bias_gradient = []
		for i in range(1, len(gradients)):
			bias_gradient.append(gradients[i] * self.blockwise_biases[i])

		return input_gradient, bias_gradient

	def _normalize(self, x, eps=1e-6):
		x = abs(x)
		x = x - x.min()
		x = x / (x.max() + eps)
		return x

	def saliency(self, image, target_class=None):
		self.model.eval()
		input_grad, bias_grad = self.fullGradientDecompose(image, target_class=target_class)

		grd = input_grad[0] * image
		gradient = self._normalize(grd).sum(1, keepdim=True)
		cam = gradient

		im_size = image.size()
		for i in range(len(bias_grad)):
			
			if len(bias_grad[i].size()) == len(im_size):
				temp = self._normalize(bias_grad[i])
				gradient = F.interpolate(temp, size=(im_size[2], im_size[3]), mode = 'bilinear', align_corners=False)
				cam += gradient.sum(1, keepdim=True)
		return cam

