#!/bin/bash
model='resnet18' # other choices are 18, 34, 50, 101 and 152
python draw_saliency.py -model $model
