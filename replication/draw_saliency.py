#! -*- coding:utf-8 -*-

# -------------------------------------------------------------------
import datetime
def report(*args):
	print(datetime.datetime.now().strftime('%Y-%m-%d/%H:%M:%S.%f')+' '+' '.join(map(str,args)).replace('\n',''))


# -------------------------------------------------------------------
import cv2
def draw(x, s, file_name):
	report(f'draw:{file_name}')
	os.makedirs(os.path.dirname(file_name),exist_ok=True)

	x = x.detach().numpy()
	s = s.detach().numpy()

	s = np.uint8(s * 255).transpose(1,2,0)
	s = cv2.resize(s, (224,224))

	x = np.uint8(x * 255).transpose(1,2,0)
	x = cv2.resize(x, (224, 224))
	
	# Apply JET colormap
	color_heatmap = cv2.applyColorMap(s, cv2.COLORMAP_JET)
	cv2.imwrite(f'{file_name}-saliency.png', color_heatmap)
	
	# Combine image with heatmap
	img_with_heatmap = np.float32(color_heatmap) + np.float32(x)
	img_with_heatmap = img_with_heatmap / np.max(img_with_heatmap)

	os.makedirs(os.path.dirname(file_name),exist_ok=True)
	cv2.imwrite(f'{file_name}-overlay.png', np.uint8(255 * img_with_heatmap))


# -------------------------------------------------------------------
from torchvision import datasets
import torch
def load_dataset(image_dir):
	transform = []
	transform.append(transforms.Resize((224,224)))
	transform.append(transforms.ToTensor())
	transform.append(transforms.Normalize(mean = [0.485, 0.456, 0.406],std = [0.229, 0.224, 0.225]))
	transform = transforms.Compose(transform)
	data = datasets.ImageFolder(image_dir, transform=transform)
	data = torch.utils.data.DataLoader(data, batch_size=1, shuffle=False)
	return data

import os
def gen_data(data_dir):
	for d in sorted(os.listdir(data_dir)):
		if '.DS' in d: continue
		yield d, load_dataset(f'{data_dir}/{d}')

# -------------------------------------------------------------------
from models import resnets
def get_model(config):
	if config.target_model=='resnet18':	 return resnets.resnet18(pretrained=True)
	if config.target_model=='resnet34':	 return resnets.resnet34(pretrained=True)
	if config.target_model=='resnet50':	 return resnets.resnet50(pretrained=True)
	if config.target_model=='resnet101': return resnets.resnet101(pretrained=True)
	if config.target_model=='resnet152': return resnets.resnet152(pretrained=True)

from torchvision import transforms
class NormalizeInverse(transforms.Normalize):
	def __init__(self, mean, std):
		mean = torch.as_tensor(mean)
		std = torch.as_tensor(std)
		std_inv = 1 / (std + 1e-7)
		mean_inv = -mean * std_inv
		super(NormalizeInverse, self).__init__(mean=mean_inv, std=std_inv)

	def __call__(self, tensor):
		return super(NormalizeInverse, self).__call__(tensor.clone())


# -------------------------------------------------------------------
from argparse import ArgumentParser
def get_config():
	config = ArgumentParser()
	config.add_argument('--data_dir',					default='DATAs',			type=str)
	config.add_argument('--output_dir',					default='SALIENCY-MAPs',	type=str)
	config.add_argument('--target_model',	'-model',	default='resnet18',			type=str, choices=[f'resnet{L}' for L in [18,34,50,101,152]])
	return config.parse_args()

# -------------------------------------------------------------------
import os
import numpy as np

from fullgrad import FullGrad
def main():
	config 	= get_config()
	model 	= get_model(config)
	denormalizer	= NormalizeInverse(mean = [0.485, 0.456, 0.406], std = [0.229, 0.224, 0.225])

	for fruit, data in gen_data(config.data_dir):
		fullgrad	= FullGrad(model)
		for i, (x,y) in enumerate(data):
			x = x.requires_grad_()

			s = fullgrad.saliency(x)
			s = s - s.min()
			s = s / s.max()
			s = s[0]

			x = denormalizer(x[0])

			file_name = f'{config.output_dir}/{fruit}/{i:03d}'
			draw(x, s, file_name)


# -------------------------------------
if __name__ == '__main__':
	main()
















# -------------------------------------