#! -*- coding:utf-8 -*-

# -------------------------------------------------------------------
import copy
import torch
def get_contribution(model, x, y):
	assert x.size(0)==1, f'input batch-size must be 1. input-shape:{x.shape}'
	
	model.eval()
	model.zero_grad()

	model_dict = model.state_dict()

	def _get_bias_in_BN(name):
		# batch-normalization : BN = static \ocot dynamic
		#  dynamimc : input -> z = (x-mean)/sqrt(var+epsilon)
		#  static   : z -> output = scale*z  + mean
		#  then, total-bias (i.e., BN(0)) is -scale * mean / sqrt(var+epsilon) + mean
		rm = model_dict[f'{name}.running_mean']
		rv = model_dict[f'{name}.running_var']
		b = model_dict[f'{name}.bias']
		w = model_dict[f'{name}.weight']
		e = 1e-05 # epsilon
		return -w*rm/torch.sqrt(rv+e) + b

	batchnorm_names = set()
	for name in model_dict.keys():
		if name.endswith('.running_mean'):
			batchnorm_names.add(name.rstrip('.running_mean'))

	x = x.requires_grad_()
	p = model(x)
	p = p[0,y]
	p.backward()

	contributions = []

	value = (x*x.grad).sum().item()
	contributions.append(value)
	for name, param in model.named_parameters():
		if name.endswith('.bias'):
			name = name.rstrip('.bias')
			if name in batchnorm_names:
				bias = _get_bias_in_BN(name)
			else:
				bias = param
			value 	= (bias*param.grad).sum().item()
			contributions.append(value)
	return contributions

# -------------------------------------------------------------------


from PIL import Image
from torchvision import transforms
def gen_dataset():
	transform = []
	transform.append(transforms.Resize((224,224)))
	transform.append(transforms.ToTensor())
	transform.append(transforms.Normalize(mean = [0.485, 0.456, 0.406],std = [0.229, 0.224, 0.225]))
	transform = transforms.Compose(transform)

	files	= ['apple.jpg','banana.jpg','orange.jpg']
	labels	= [948, 954, 950]
	for f,y in zip(files, labels):
		x = Image.open(f'DATAs/{f}')	# load image
		x = transform(x)				# pre-process
		x = x.unsqueeze(0)				# convert into one-batch
		yield f,x,y

# -------------------------------------------------------------------
def main():
	model_name = 'resnet18' # other choices are 34, 50, 101, 152
	model = torch.hub.load('pytorch/vision:v0.5.0', model_name, pretrained=True)
	for f,x,y in gen_dataset():
		print(f'test file:{f}')

		contributions = get_contribution(model, x, y)
		for i,c in enumerate(contributions):
			print(f'\tlayer:{i:03d} contribution:{c}')
		c = sum(contributions)
		print(f'\tsum-contributions: c={c:011.7f}')

		o = model(x)[0,y].item()
		print(f'\ttrue-output:       o={o:011.7f}')
		print(f'\tdiff: o-c={o-c:011.7f}')
		print('-'*50)


# -------------------------------------
if __name__ == '__main__':
	main()
















# -------------------------------------